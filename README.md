# Motion Detector
## A simple home made surveillance system for Raspberry PI

This is a Python script that detects movements around its area using a PIR sensor, takes screenshots with an usb camera and sends notifications via a **Telegram** bot.
  
  
### Requirements  
- a Raspberry PI 2/3  
- a PIR motion detector  
- an USB webcam; I used a HP webcam 1300 but it should work with any linux-compatible model  

### Hardware setup  
- Connect the PIR sensor (see *guide/pir.jpg*); **beware**: depending on the PIR manifacturer, you could need to swap the SV and the GRN wires  
- Adjust the sensitivity and the timeout according to your needs (see *guide/pir_adjust.jpg*)  
- Plug-in the webcam

### Install libraries  

	sudo apt-get install python-pip git fswebcam python-picamera python3-picamera python-rpi.gpio ntp --fix-missing  
	sudo usermod -a -G video $USER  
	pip install requests

Run raspi-config to adjust your timezone  

 
	sudo raspi-config

Clone the repository anywhere you want (suggested: **/home/pi**)  
	  
	git clone https://dagord@bitbucket.org/dagord/motion-detector.git

Create the directory where to place the screeshots  
  
	mkdir /home/pi/images

### Create a Telegram bot  

This part assumes that you already have a Telegram account:  
- Search for @BotFather from your Telegram app and start a conversation  
- type **/newbot**  
- choose a name for your bot  
- choose a username  (it should end with 'bot')  
- you will get an API token: write it down

### Software setup  

Copy/rename the **config.py.sample** to **config.py**, open it and set the necessary parameters:  

	TOKEN = "xxxx"  # the API token you got from @BotFather
	bot_name = "mybot"  # the name of your bot
	bot_user = "mybotuser_bot"  # the username
	USERNAMES = ['sam', 'tom']  # the allowed Telegram users
	CHAT_ID = ""  # the default CHAT ID (see below)
	USERNAME = 'sam'  # the default USER (see below)
	COMMANDS = ['hi', 'start', 'stop', 'status', 'photo', 'info']  # the available commands
	PHOTO_DIRECTORY = '/home/pi/images'  # the directory where to save the screenshots
	
When a new user starts a conversation with the bot, a new chat (with its unique ID) is created; if the script is restarted for whatever reason (eg. a power loss), it doesn't keep memory of the opened chats and the connected users, so it would be pretty unuseful!
Entering a default CHAT_ID and USERNAME let the script to restart the conversation.

### Launch the script

Simply type 

	python motion-detector.py

The service will start; detection mode is enabled by default.

To get the current CHAT_ID, just send a message to your bot and have a look at the logs; you should see a line such as

	16-27-31 - 123456 - New message: hi

**123456** is the id of the chat: set the CHAT_ID parameter to this value into your **config.py** file

If you want to redirect the output to a logfile, just run

        python motion-detector.py > motion.log

If you want to run the script at startup, simply place the script into your system crontab

### Available commands

- hi: returns a welcome message
- start: enables detection mode
- stop: disables detection mode
- info: returns generic informations (service uptime, detection mode status...)
- photo: take a screenshot

If detection mode is disabled the script will ignore any movement.
