#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
 Motion Detector - motion-detector.py

 Copyright 2019 Matteo D'Agord <matteo.dagord@gmail.com>

    Motion Detector is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Strimy Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Strimy Client.  If not, see <http://www.gnu.org/licenses/>.
'''

__author__ = 'Matteo D\'Agord'
__copyright__ = 'Copyright 2019, Matteo D\'Agord'
__credits__ = []
__license__ = 'GPLv3'
__version__ = '1.1'
__maintainer__ = 'Matteo D\'Agord'
__email__ = 'matteo.dagord@gmail.com'
__status__ = 'Production'

import os, time
from datetime import datetime
try:
	from gpiozero import MotionSensor
	debug = False
except:
	debug = True

from threading import Thread
from bot import Bot
import config

path = os.path.dirname(os.path.abspath(__file__))

class MotionDetector(Bot):

	def __init__(self):
		self.server_started = True
		self.server_changed = False
		self.start_time = datetime.now()

		try:
			thMonitor._stop()
		except:
			pass
		thMonitor = Thread(target=self.Monitor)
		thMonitor.start()

	def ReadCommand(self, line):
		if line != '':
			print "command received by script: %s" % line
			if line == 'start':
				self.server_started = True
				self.server_changed = True
				print "Detection mode started"
				self.SendMessage(self.ServiceStatus())
			elif line == 'stop':
				self.server_started = False
				self.server_changed = True
				print "Detection mode stopped"
				self.SendMessage(self.ServiceStatus())
			elif line == 'photo':
				if debug:
					self.SendMessage('Running in debug mode, couldn\'t take photo')
				else:
					prefix_date, prefix_time = self.GetDateTime()
					imagepath = self.TakePhoto(prefix_date, prefix_time)
					self.SendImage(imagepath)
			elif line == 'info':
				self.SendMessage('Server started at %s\n%s' % (self.start_time.strftime('%Y-%m-%d %H:%M:%S'), self.ServiceStatus()))

	def ServiceStatus(self):
		if self.server_started:
			return "Detection mode started"
		else:
			return "Detection mode stopped"

	def SendMessage(self, text):
		self.SendMessageToBot(text, config.CHAT_ID, config.USERNAME)

	def SendImage(self, imagepath):
		self.SendImageToBot(imagepath, config.CHAT_ID, config.USERNAME)

	def TakePhoto(self, prefix_date, prefix_time):
		try:
			os.mkdir('%s/%s' % (config.PHOTO_DIRECTORY, prefix_date))
		except:
			pass
		imagepath = "%s/%s/%s_image.jpg" % (config.PHOTO_DIRECTORY, prefix_date, prefix_time)

		command = "fswebcam -q -r 768x576 --set brightness=60 %s" % imagepath
		line = "%s %s - Shot taken! (%s)" % (prefix_date, prefix_time.replace('-',':'), imagepath)
		os.system(command)
		print line
		return imagepath

	def GetDateTime(self):
		now = datetime.now()
		prefix_date = "%s-%s-%s" % (now.year, now.month, now.day)
		prefix_time = "%s-%s-%s" % (now.hour, now.minute, now.second)
		return prefix_date, prefix_time

	def Monitor(self):
		if not debug:
			pir = MotionSensor(4)

			print "Server started"
			file = open("motion.log", "a+")
			while True:

				if self.server_started:
					pir.wait_for_motion()

					if self.server_started:
						prefix_date, prefix_time = self.GetDateTime()
						line = "%s %s - motion detected!" % (prefix_date, prefix_time.replace('-',':'))
						self.SendMessage(line)
						imagepath = self.TakePhoto(prefix_date, prefix_time)

						self.SendImage(imagepath)

						file.write(line +"\n")
						file.flush()

					else:
						print "Motion detection disabled"

				time.sleep(10)

motion = MotionDetector()
motion.InitBot()
if debug:
	motion.SendMessage("Server started in debug mode")
else:
	motion.SendMessage("Server started")
