#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
 Motion Detector - bot.py

 Copyright 2019 Matteo D'Agord <matteo.dagord@gmail.com>

    Motion Detector is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Strimy Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Strimy Client.  If not, see <http://www.gnu.org/licenses/>.
'''

__author__ = 'Matteo D\'Agord'
__copyright__ = 'Copyright 2019, Matteo D\'Agord'
__credits__ = []
__license__ = 'GPLv3'
__version__ = '1.1'
__maintainer__ = 'Matteo D\'Agord'
__email__ = 'matteo.dagord@gmail.com'
__status__ = 'Production'


import json
import requests
import time
import os
from threading import Thread
import config
URL = "https://api.telegram.org/bot{}/".format(config.TOKEN)


class Bot:

	def InitBot(self):
		global config

		thread_started = False
		last_update_id = None

		if not thread_started:
			try:
				th._stop()
			except:
				pass
			th = Thread(target=self._listen_messages) # , args=(config.CHAT_ID, config.USERNAME))
			th.start()
			thread_started = True

	def _listen_messages(self):
	    last_update_id = None
	    while True:
	        updates = self._get_updates(last_update_id)
	        if "result" in updates:
	                if len(updates["result"]) > 0:
	                    last_update_id = self._get_last_update_id(updates) + 1
	                    config.CHAT_ID = updates["result"][0]["message"]["chat"]["id"]
	                    config.USERNAME = updates["result"][0]["message"]["from"]["username"]

			    self._echo_all(updates, config.USERNAME)
		time.sleep(1)

	def _get_url(self, url):
	    retry = 0
	    content = ""
	    response_ok = False

	    while not response_ok and retry < 15:
	    	try:
		    response = requests.get(url, timeout=(2,5))
		    content = response.content.decode("utf8")
		    response_ok = True
		except:
		    retry += 1
		    time.sleep(2)
	    return content

	def _get_json_from_url(self, url):
	    content = self._get_url(url)
	    js = json.loads(content)
	    return js

	def _get_updates(self, offset=None):
	    url = URL + "getUpdates"
	    if offset:
	        url += "?offset={}".format(offset)
	    js = self._get_json_from_url(url)
	    return js

	def _get_last_chat_id_and_text(self, updates):
	    num_updates = len(updates["result"])
	    last_update = num_updates - 1
	    text = updates["result"][last_update]["message"]["text"]
	    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
	    return (text, chat_id)

	def _get_last_update_id(self, updates):
	    update_ids = []
	    for update in updates["result"]:
	        update_ids.append(int(update["update_id"]))
	    return max(update_ids)

	def SendMessageToBot(self, text, chat_id, username):
	    if chat_id != '' and username in config.USERNAMES:
		    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
		    self._get_url(url)

	def SendImageToBot(self, image, chat_id, username):
	    if chat_id != '' and username in config.USERNAMES:
		    url = URL + "sendPhoto";
		    files = {'photo': open(image, 'rb')}
		    data = {'chat_id' : chat_id}
		    r= requests.post(url, files=files, data=data, timeout=10)

	def _echo_all(self, updates, username):
	    for update in updates["result"]:
	        try:
	            text = update["message"]["text"]
	            chat = update["message"]["chat"]["id"]
		    text = text.lower()
		    prefix_date, prefix_time = self.GetDateTime()
		    print "%s - %s - New message: %s" % (prefix_time, chat, text)
		    if text in config.COMMANDS:
				self._parse_command(text, chat, username)
		    else:
				self.SendMessageToBot("command unknown\nAvailable commands: %s" % config.COMMANDS, chat, username)
	        except Exception as e:
	            print(e)

	def _parse_command(self, text, chat, username):
		if text == 'hi':
			self.SendMessageToBot("Welcome!\nAvailable commands: %s" % config.COMMANDS, chat, username)
		elif text == "start" or text == "stop" or text == "status" or text == "photo" or text == "info":
			self.ReadCommand(text)
